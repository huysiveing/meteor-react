import { Mongo } from 'meteor/mongo';
import SimpleSchema2Bridge from 'uniforms-bridge-simple-schema-2';
import SimpleSchema from 'simpl-schema';

const Category = new Mongo.Collection('category');

// Create a schema to specify the structure of the data to appear in the form.
const formSchema = new SimpleSchema({
    name: String,
    status: {
        type: Boolean,
        label: 'Use or Not',
        defaultValue: true
    },
});

const CategoryForm = new SimpleSchema2Bridge(formSchema);

export { Category, CategoryForm };