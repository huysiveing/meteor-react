import React from 'react'
import { Button, Header, Icon, Modal } from 'semantic-ui-react'

function _Modal(text) {

    const data = text;
    const [open, setOpen] = React.useState(false)

    return (
        <Modal
            basic
            onClose={() => setOpen(false)}
            onOpen={() => setOpen(true)}
            open={open}
            size='small'
            trigger={<Button> {data.btn_title} </Button>}
        >
            <Header icon>
                <Icon name='archive' />
                {data.title}
            </Header>
            <Modal.Content>
                <p>
                    {data.msg}
                </p>
            </Modal.Content>
            <Modal.Actions>
                {data.no ?
                    <Button basic color='red' inverted onClick={() => setOpen(false)}>
                        <Icon name='remove' /> No
                    </Button>
                :''}

                {data.yes ?
                    <Button color='green' inverted onClick={() => setOpen(false)}>
                        <Icon name='checkmark' /> Yes
                    </Button>
                :''}
              
               
            </Modal.Actions>
        </Modal>
    )
}

export default _Modal