import React from 'react';
import { Button, Table } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import _DeletePopup from './DeletePopup';

class CategoryItem extends React.Component {
    render() {
        return (
            <Table.Row>
                <Table.Cell>{this.props.index + 1}</Table.Cell>
                <Table.Cell>{this.props.categoryData.name}</Table.Cell>
                <Table.Cell>{this.props.categoryData.status == true ? 'Use' : 'No Use'}</Table.Cell>
                <Table.Cell  textAlign='center'>
                    <Button primary as={NavLink} to={`/admin/category/edit/${this.props.categoryData._id}`}>Edit</Button>
                </Table.Cell>
                <Table.Cell textAlign='center'>
                    {/* <Button negative _id={this.props.categoryData._id} onClick={this.deleteCategory}>Delete</Button> */}
                    <_DeletePopup
                        _id={this.props.categoryData._id}
                        btn_title="Delete"
                        color='red'
                        collection='Category'
                        title="Confirmation"
                        msg="Are you sure to remove this item?"
                        yes={true}
                        no={true}
                    />
                </Table.Cell>
            </Table.Row>
        );
    }
}

// Require a document to be passed to this component.
CategoryItem.propTypes = {
    categoryData: PropTypes.shape({
        name: PropTypes.string,
        status: PropTypes.bool,
        _id: PropTypes.string,
    }).isRequired,
};

export default CategoryItem;
