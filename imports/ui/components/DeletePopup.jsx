import React from 'react'
import { Button, Header, Icon, Modal } from 'semantic-ui-react'
import swal from 'sweetalert';

function _DeletePopup(text) {

    const data = text;
    const [open, setOpen] = React.useState(false);

    return (
        <Modal
            basic
            onClose={() => setOpen(false)}
            onOpen={() => setOpen(true)}
            open={open}
            size='small'
            trigger={<Button color={data.color} > {data.btn_title} </Button>}
        >
            <Header icon>
                <Icon name='archive' />
                {data.title}
            </Header>

            <Modal.Content>
                {data.msg}
            </Modal.Content>

            <Modal.Actions>
                {data.no ?
                    <Button basic color='red' inverted onClick={() => setOpen(false)}>
                        <Icon name='remove' /> No
                    </Button>
                :''}

                {data.yes ?
                    <Button color='green' inverted onClick={() => { DeleteData(data._id, data.collection)}}>
                        <Icon name='checkmark' /> Yes
                    </Button>
                :''}
            </Modal.Actions>
        </Modal>
    )
}

function DeleteData(id, table){
    if(table !== "" && id !== undefined){
        Meteor.call('DeleteMethod', table, id, (err,rs) =>{
            if (rs) {
                swal('Success', 'Item deleted successfully', 'success');
            } else {
                swal('Error', 'Something Error', 'error');
            }
        })
    }
}

export default _DeletePopup