import React from 'react';
import { Table } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';

class MemberItem extends React.Component {
    render() {
        return (
            <Table.Row>
                <Table.Cell>{this.props.index + 1}</Table.Cell>
                <Table.Cell>{this.props.user.username}</Table.Cell>
            </Table.Row>
        );
    }
}

// Require a document to be passed to this component.
MemberItem.propTypes = {
    user: PropTypes.shape({
        username: PropTypes.string,
        _id: PropTypes.string,
    }).isRequired,
};

export default MemberItem;
