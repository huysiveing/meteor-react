import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { Container, Dropdown, Input, Menu, Segment } from 'semantic-ui-react';
import { NavLink, withRouter } from 'react-router-dom'
import { withTracker } from 'meteor/react-meteor-data';

class NavBar extends React.Component {

    constructor(props) {
        super(props);
    }

    state = { activeItem: 'home' }

    handleItemClick = (e, { name }) => this.setState({ activeItem: name });
   
    render() {
        const { activeItem } = this.state

        return (
            <Segment inverted>
                <Container>
                    <Menu inverted secondary>
                        <Menu.Item
                            as={NavLink}
                            name='home'
                            to="/"
                            active={activeItem === 'home'}
                            onClick={this.handleItemClick}
                        />

                        {/* Admin Menu */}
                        {Roles.userIsInRole(Meteor.userId(), 'admin') ? (
                            [
                                <Menu.Item as={NavLink} activeClassName="active" exact to="/admin/dashboard" key='admin_dashboard'>Admin Dashboard</Menu.Item>,
                                <Menu.Item as={NavLink} activeClassName="active" exact to="/admin/member/list" key='admin_memebrlist'>Member List</Menu.Item>,
                                <Menu.Item as={NavLink} activeClassName="active" exact to="/admin/category" key='admin_category'>Category</Menu.Item>
                            ]
                        ) : ''}

                        {/* User Menu */}
                        {Roles.userIsInRole(Meteor.userId(), 'user') ? (
                            <Menu.Item as={NavLink} activeClassName="active" exact to="/dashboard" key='dashboard'>User Dashboard</Menu.Item>
                        ) : ''} 

                        <Menu.Item position="right">
                            {this.props.currentUser === '' ? (
                                <Dropdown id="login-dropdown" text="Login" pointing="top right" icon={'user'}>
                                    <Dropdown.Menu>
                                        <Dropdown.Item id="login-dropdown-sign-in" icon="user" text="Sign In" as={NavLink} exact to="/signin" />
                                        <Dropdown.Item id="login-dropdown-sign-up" icon="add user" text="Sign Up" as={NavLink} exact to="/signup" />
                                    </Dropdown.Menu>
                                </Dropdown>
                            ) : (
                                <Dropdown id="navbar-current-user" text={this.props.currentUser} pointing="top right" icon={'user'}>
                                    <Dropdown.Menu>
                                        <Dropdown.Item id="navbar-sign-out" icon="sign out" text="Sign Out" as={NavLink} exact to="/signout" />
                                    </Dropdown.Menu>
                                </Dropdown>
                            )}
                        </Menu.Item>
                    </Menu>
                </Container>
            </Segment>
        );
    }
}

// Declare the types of all properties.
NavBar.propTypes = {
    currentUser: PropTypes.string
};

const NavBarContainer = withTracker(() => ({
    currentUser: Meteor.user() ? Meteor.user().username : '',
}))(NavBar);

export default withRouter(NavBarContainer);
// export default NavBarContainer;