import React from 'react';
import { Container, Table, Header, Loader } from 'semantic-ui-react';
import NavBar from './NavBar';
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import 'semantic-ui-css/semantic.css';
import Footer from '../pages/Footer';
import RouterConfig from '../../startup/client/Router';

class App extends React.Component {

    render() {
        return (
            <Router>
                {/* Master navigation layout */}
                <NavBar />

                {/* Router config */}
                <RouterConfig />

                {/* Footer layout */}
                <Footer />
            </Router>
        );
    }
}

export default App;