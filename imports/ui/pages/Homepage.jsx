import React, { useState } from 'react'
import { withRouter } from 'react-router-dom';
import { Table, Flag, Message, Container, Button, Segment, Dimmer, Loader } from 'semantic-ui-react'
import _Modal from '../components/Modal';


class Homepage extends React.Component {

    render() {
        return (
            <Container>

                <_Modal
                    btn_title="Show Modal"
                    title="Hello from siveing"
                    msg="Would you like us to enable automatic archiving of old messages?"
                    yes={true}
                    no={true}
                />

                <Message
                    error
                    header='There was some errors with your submission'
                    list={[
                        'You must include both a upper and lower case letters in your password.',
                        'You need to select your home country.',
                    ]}
                />

                <Button fluid primary>Siveing</Button>
                <Message color='red'>Red</Message>
                <Message color='orange'>Orange</Message>
            </Container>
        );
    }
}

export default withRouter(Homepage);
