import React from 'react';
import { Grid, Segment, Header, Table, Container } from 'semantic-ui-react';
import { AutoForm, BoolField, ErrorsField, NumField, SelectField, SubmitField, TextField } from 'uniforms-semantic';
import swal from 'sweetalert';
import { Meteor } from 'meteor/meteor';
import { Category, CategoryForm } from '../../../../api/collection/category';
import CategoryItem from '../../../components/CategoryItem';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';

class AddCategory extends React.Component {

    // On submit, insert the data.
    submit(data, formRef) {
        const { name, status } = data;
        Category.insert({ name, status},
            (error) => {
                if (error) {
                    swal('Error', error.message, 'error');
                } else {
                    swal('Success', 'Item added successfully', 'success');
                    formRef.reset();
                }
            });
    }

    // On submit, update the data.
    submitEdit(data) {
        const { name, status, _id } = data;
        Category.update(_id, { $set: { name, status} }, 
            (error) => {
                if (error) {
                    swal('Error', error.message, 'error');
                } else {
                    swal('Success', 'Item updated successfully', 'success');
                    this.props.history.push('/admin/category');
                }
            });
    }

    render() {
        let fRef = null;
        return (
            <Container>
                {/* Form Category */}
                <Grid centered>
                    <Grid.Column>
                        <Header as="h2" textAlign="center">Add Category</Header>

                        { this.props.categoryId !== undefined
                        ? 
                            (
                                <AutoForm model={this.props.categoryDoc} schema={CategoryForm} onSubmit={dataDoc => this.submitEdit(dataDoc)} >
                                    <Segment>
                                        <TextField name='name' />
                                        <BoolField name='status' />
                                        <SubmitField value='Submit' />
                                        <ErrorsField />
                                    </Segment>
                                </AutoForm>
                            )
                        : 
                            (
                                <AutoForm ref={ref => { fRef = ref; }} schema={CategoryForm} onSubmit={data => this.submit(data, fRef)} >
                                    <Segment>
                                        <TextField name='name' />
                                        <BoolField name='status' />
                                        <SubmitField value='Submit' />
                                        <ErrorsField />
                                    </Segment>
                                </AutoForm>
                            )
                        }
                      
                    </Grid.Column>
                </Grid>
                
                {/* List Category */}
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>No</Table.HeaderCell>
                            <Table.HeaderCell>Name</Table.HeaderCell>
                            <Table.HeaderCell>Use or Not</Table.HeaderCell>
                            <Table.HeaderCell>Edit</Table.HeaderCell>
                            <Table.HeaderCell>Delete</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {this.props.category.map((catData, index) => <CategoryItem key={catData._id} index={index} categoryData={catData} />)}
                    </Table.Body>
                </Table>
            </Container>
        );
    }
}

// Require an array of Category documents in the props.
AddCategory.propTypes = {
    category: PropTypes.array.isRequired,
    categoryDoc: PropTypes.object,
    categoryId: PropTypes.string
};

export default withTracker(({match}) => {

    const categoryId = match.params._id !== '' ? match.params._id : null;
    // Get access to Category documents.
    const subscription = Meteor.subscribe("PublishCategoryAll");
    // Determine if the subscription is ready
    const ready = subscription.ready();
    // Get the Category documents
    const category = Category.find({}).fetch();
    const categoryDoc = match.params._id !== '' ? Category.findOne({_id:  match.params._id}) : null;

    return {
        category,
        categoryDoc,
        categoryId
    };
})(AddCategory);