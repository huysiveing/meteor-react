import React from 'react'
import { Container, Message } from 'semantic-ui-react'

const AdminDashboard = () => {
    return (
        <Container>
            <Message color='blue'>Admin Dashbaord</Message>
        </Container>
    )
}

export default AdminDashboard