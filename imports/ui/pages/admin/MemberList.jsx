import React from 'react'
import { Container, Header, Loader, Message, Table } from 'semantic-ui-react'
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import MemberItem from '../../components/MemberItem';

class MemberList extends React.Component {

    // If the subscription(s) have been received, render the page, otherwise show a loading icon.
    render() {
        return (this.props.ready) ? this.renderPage() : <Loader active>Getting data</Loader>;
    }

    // If the subscription(s) have been received, render the page, otherwise show a loading icon.
    renderPage() {
        return (
            <Container>
                <Header as="h2" textAlign="center">List Members</Header>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>No</Table.HeaderCell>
                            <Table.HeaderCell>Email</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {this.props.users.map((userData, index) => <MemberItem key={userData._id} index={index} user={userData} />)}
                    </Table.Body>
                </Table>
            </Container>
        )
    }
}


// Require an array of Stuff documents in the props.
MemberList.propTypes = {
    users: PropTypes.array.isRequired,
};

// withTracker connects Meteor data to React components. https://guide.meteor.com/react.html#using-withTracker
export default withTracker(() => {

    // Get access to users documents.
    const subscription = Meteor.subscribe('PublishMemberAll');
    // Determine if the subscription is ready
    const ready = subscription.ready();

    // Get the user documents
    const users = Meteor.users.find({}).fetch();
    return {
        users,
        ready
    };
})(MemberList);
