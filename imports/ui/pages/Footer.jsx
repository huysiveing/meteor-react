import React from 'react';

/** The Footer appears at the bottom of every page. Rendered by the App Layout component. */
class Footer extends React.Component {
  render() {
    const divStyle = { paddingTop: '15px' };
    return (
      <footer>
        <div style={divStyle} className="ui center aligned container">
            <hr />
                Department of software development <br />
            <a href="https://info.siveing-dev.com">Siveing</a>
        </div>
        <br />
      </footer>
    );
  }
}

export default Footer;
