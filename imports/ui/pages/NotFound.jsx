import React from 'react';
import { NavLink } from 'react-router-dom';
import { Button, Container, Header, Message } from 'semantic-ui-react';

/** Render a Not Found page if the user enters a URL that doesn't match any route. */
class NotFound extends React.Component {
    render() {
        return (
            <Container textAlign="center">
                <Header as="h2" textAlign="center">
                    <p>Page not found</p>
                </Header>
                <Button as={NavLink} to="/" primary>Go to home</Button>
            </Container>
        );
    }
}
export default NotFound;
