import React from 'react';
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

// Default
import Homepage from '../../ui/pages/Homepage';
import NotFound from '../../ui/pages/NotFound';

// Auth
import Signin from '../../ui/pages/auth/Signin';
import Signout from '../../ui/pages/auth/Signout';
import Signup from '../../ui/pages/auth/Signup';

// Dahsboard
import AdminDashboard from '../../ui/pages/admin/Dashboard';
import UserDashboard from '../../ui/pages/user/Dashboard';
import MemberList from '../../ui/pages/admin/MemberList';
import AddCategory from '../../ui/pages/admin/category/AddCategory';

function RouterConfig() {
    return (
        <Switch>
            {/* Public route */}
            <Route exact path="/" component={Homepage} />
            <AfterLoginRoute path="/signin" component={Signin} />
            <AfterLoginRoute path="/signup" component={Signup} />
            <Route path="/signout" component={Signout} />

            {/* Admin Route */}
            <AdminProtectedRoute exact path="/admin/dashboard" component={AdminDashboard} />
            <AdminProtectedRoute exact path="/admin/member/list" component={MemberList} />
            <AdminProtectedRoute exact path="/admin/category" component={AddCategory} />
            <AdminProtectedRoute exact path="/admin/category/edit/:_id" component={AddCategory} />

            {/* User Route */}
            <ProtectedRoute exact path="/dashboard" component={UserDashboard} />

            {/* error page */}
            <Route component={NotFound} />
        </Switch>
    );
}

const AfterLoginRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={(props) => {
            const isLogged = Meteor.userId() !== null;
            return isLogged ?
                (<Redirect to={{ pathname: '/', state: { from: props.location } }} />):
                (<Component {...props} />) ;
        }}
    />
);

const ProtectedRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={(props) => {
            const isLogged = Meteor.userId() !== null;
            return isLogged ?
                (<Component {...props} />) :
                (<Redirect to={{ pathname: '/signin', state: { from: props.location } }} />
                );
        }}
    />
);

/**
 * AdminProtectedRoute (see React Router v4 sample)
 * Checks for Meteor login and admin role before routing to the requested page, otherwise goes to signin page.
 * @param {any} { component: Component, ...rest }
 */
const AdminProtectedRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={(props) => {
            const isLogged = Meteor.userId() !== null;
            const isAdmin = Roles.userIsInRole(Meteor.userId(), 'admin');
            return (isLogged && isAdmin) ?
                (<Component {...props} />) :
                (<Redirect to={{ pathname: '/signin', state: { from: props.location } }} />
                );
        }}
    />
);

AfterLoginRoute.propTypes = {
    component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
    location: PropTypes.object,
};

ProtectedRoute.propTypes = {
    component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
    location: PropTypes.object,
};

// Require a component and location to be passed to each AdminProtectedRoute.
AdminProtectedRoute.propTypes = {
    component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
    location: PropTypes.object,
};

export default RouterConfig;