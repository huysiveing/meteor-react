import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { Category } from '../../api/collection/category';

// // Admin-level publication.
// // If logged in and with admin role, then publish all documents from all users. Otherwise publish nothing.
Meteor.publish('PublishMemberAll', function() {
    if (this.userId && Roles.userIsInRole(this.userId, 'admin')) {
        return Meteor.users.find();
    }
    return this.ready();
});

Meteor.publish('PublishCategoryAll', function() {
    return Category.find();
});


// alanning:roles publication
// Recommended code to publish roles for each user.
Meteor.publish(null, function() {
    if (this.userId) {
        return Meteor.roleAssignment.find({ 'user._id': this.userId });
    }
    return this.ready();
});