import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';

Meteor.methods({
    AddUserRole: function(userId) {
        if (userId !== undefined) {
            Roles.addUsersToRoles(userId, 'user');
            return true;
        }
        return false;
    }
});

// Create initail role 
Meteor.startup(function() {
    let roles = ['admin', 'user'];
    if (Meteor.roles.find().count() === 0) {
        roles.map(function(role) {
            Roles.createRole(role)
        })
    };
});

// Update role to user manual
// let user = Accounts.findUserByEmail("admin@gmail.com");
// Roles.setUserRoles(user._id, ['admin']);