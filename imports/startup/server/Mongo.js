import { Meteor } from 'meteor/meteor';
import { Category } from '../../api/collection/category';

// Initialize the database with a default data document.
// function addData(data) {
//     console.log(`  Adding: ${data.name} (${data.owner})`);
//     Stuffs.collection.insert(data);
// }

// Initialize the StuffsCollection if empty.
// if (Stuffs.collection.find().count() === 0) {
//     if (Meteor.settings.defaultData) {
//         console.log('Creating default data.');
//         Meteor.settings.defaultData.map(data => addData(data));
//     }
// }
// function addData(data) {
//     Category._collection.insert(data);
// }
// Category.insert({ name: 'siveing', quantity: 1, condition: 1 })
Meteor.methods({
    DeleteMethod: (table, id) => {
        if (table == "Category") {
            if (!Roles.userIsInRole(Meteor.userId(), 'admin')) return false;
            return Category.remove(id);
        }
        return false;
    }
})